import numpy as np 
import random 

# Firsty, we need to import two Python 3 libraries namely 
# numpy and random. Numpy is used for the matrix formation
# and matrix operations in this program and random is used
# for generating the random values of integers that we want 
# to put in our matrices A and b for the solution of Ax=b.

def gauss_seidel(matrix_a,matrix_b,x_0):
    L_star=np.empty((4,4))
    U=np.empty((4,4))
    for i in range (4):
        for j in range (4):
            if j<=i:
                L_star[i][j]=matrix_a[i][j]
                U[i][j]=0
            else:
                L_star[i][j]=0
                U[i][j]=matrix_a[i][j]
    L_star=np.linalg.inv(L_star)
    T=-np.matmul(L_star,U)
    C=np.matmul(L_star,matrix_b) 
    x=np.matmul(T,x_0)+C 
    return x

# Now we define our iteration function. Firstly at line 10, we 
# initialize our matrix L_star and at line 11 we initialize U. 
# Then at the loops in line 13 to 20, we fill in the blanks of 
# L_star and U that is we create a lower triangular matrix L_star
# and an upper triangular matrix U from our initial matrix matrix_a.
# Then at lines 21 through 24, we create the values necessary to find x
# iteratively. Finally, we return the value of x from our function.
   
A=np.empty((4,4))
b=np.empty((4,1))
x=np.array([[1],[1],[1],[1]])
for i in range (4):
    for j in range (4):
        A[i][j]=random.randint(1,100)
for i in range (4):
    b[i][0]=random.randint(1,100)
print(A)
for i in range (25):
    x=gauss_seidel(A,b,x)
    print('The {}. iteration is: \n {}'.format(i,x))

# Now at the main body of our function, we first initialize A and b,
# then we write down our best guess for x which is here the 4x1 matrix 
# comprised of ones. (This guess can be changed to whatever the final 
# user wants.) Afterwards, with the help of random, we fill in the blanks
# in our matrices A and b with random integers ranging from 1 to 100. 
# After all of this, we first print out the matrix A, then according to 
# the number of iterations we want, (this is also customizable) we print
# the result of our function i.e iteration. 

# Note: Many times I have tried this function it actually does not converge
# because of the fact that oftimes A is not a diagonally dominant matrix. 
# Thus I first tried to make the program work in a way that only diagonally
# dominant matrices will enter and execute the function. However, this increased
# the elapsed time for the program to work to around 1 minutes which is not practical
# at all. Therefore, I did not include the diagonal dominance part of the program/script.