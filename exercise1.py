import matplotlib.pyplot as plt 
import portion as P 
import math

# We imported three Python 3 libraries of which matplotlib.pyplot 
# is for graphing our solution, portion is for the representation
# of our interval of the values of R_0 that we want our graph to 
# represent. Finally, math library is for defining and representing 
# special functions such as exp.  

def equation(R_0, tau):
    init_eq=math.exp(-R_0*tau)+tau-1 
    return init_eq 

# We first define our function of 2 variables and pass it 
# to a variable called init_eq and return that variable 
# whenever values for R_0 and tau are given. 

def bisection_method(a,b,tolerance,R_0_startpoint,R_0_endpoint):
    solution_list=[]
    list_R_0=list(P.iterate(P.closed(R_0_startpoint,R_0_endpoint), step=0.01))
    
# We first create our interval accordingly with the 2 parameters given
# in our bisection_method function to determine the boundaries of our interval 
# and with the help of portion library we create a partition of our interval
# in which the length between two elements is given by the parameter step.
    
    for i in list_R_0:
        if equation(i,a)*equation(i,b)>0:
            continue

# Here we create a for loop to iterate through all the elements of our 
# interval for R_0. Then right after that we put the bisection method
# into work that is if for a constant R_0, (i in our loop) the tau values
# of a and b plugged into the equation and multiplicated gives us a value 
# greater than zero, we simply say go on with the rest of the code. 

        else:
            if equation(i,a)==0:
                solution_list.append(a)
                if i>1:
                    a=0.0001

# However, if the result of the multiplication above is less than zero, 
# we first check if we get a zero in the endpoints of our interval. 
# If that is the case, (particularly in this example, a=0 is a root)
# we put the value into our solutions list. Then, if i>1, (i.e R_0>1)
# we increment the a value just a little bit to avoid getting zero all
# the time since for R_0>1, there exists a root other than zero. 

                if equation(i,a)*equation(i,b)<0:   
                    temp1=a
                    temp2=b 
                    while (temp2 - temp1)/2.0 > tolerance:
                        midpoint = (temp1 + temp2)/2.0
                        if equation(i,midpoint) == 0:
                            solution_list.append(midpoint)
                        elif equation(i,temp1)*equation(i,midpoint) <= 0: 
                            temp2 = midpoint
                        else:
                            temp1 = midpoint
                    if equation(i,temp1)<tolerance:
                        solution_list.append(temp1)
                    elif equation(i,temp2)<tolerance:
                        solution_list.append(temp2)

# Here, we employ the good old bisection algorithm and get the approximations
# smaller than the tolerance that is passed as a parameter of our function.

                else:
                    pass

# Here, again we simply say go on with the rest of the code. 

            elif equation(i,b)==0:
                if equation(i,a)*equation(i,b)<0:   
                    temp1=a
                    temp2=b 
                    while (temp2 - temp1)/2.0 > tolerance:
                        midpoint = (temp1 + temp2)/2.0
                        if equation(i,midpoint) == 0:
                            solution_list.append(midpoint)
                        elif equation(i,temp1)*equation(i,midpoint) <= 0: 
                            temp2 = midpoint
                        else:
                            temp1 = midpoint
                    if equation(i,temp1)<tolerance:
                        solution_list.append(temp1)
                    elif equation(i,temp2)<tolerance:
                        solution_list.append(temp2)

# Just like the case with equation being zero when a is plugged in, we 
# do the same things for the case when b is plugged into the equation and 
# we get a zero.

            else:
                if equation(i,a)*equation(i,b)<0:   
                    temp1=a
                    temp2=b 
                    while (temp2 - temp1)/2.0 > tolerance:
                        midpoint = (temp1 + temp2)/2.0
                        if equation(i,midpoint) == 0:
                            solution_list.append(midpoint)
                        elif equation(i,temp1)*equation(i,midpoint) <= 0: 
                            temp2 = midpoint
                        else:
                            temp1 = midpoint
                    if equation(i,temp1)<tolerance:
                        solution_list.append(temp1)
                    elif equation(i,temp2)<tolerance:
                        solution_list.append(temp2)
    return solution_list

# Then we employ the bisection algorithm again to get approximations. All 
# the approximations are written to a list called solution_list which is 
# returned by the function.
            
interval_R_0=P.closed(0,5)

# Here we define the interval of values for R_0

list_R_0=list(P.iterate(P.closed(0,5), step=0.00998))

# Then we iterate through the interval by a given step
# which is basically the distance between two consecutive
# elements of our list. Here the value was chosen so that 
# the length of list_R_0 and list_tau (to be defined soon)
# are the same so that we can draw our graph. Otherwise we
# get an error.

list_tau_1=bisection_method(0,1,0.0001,0,1)
list_tau_2=bisection_method(0,1,0.0001,1,5)

# In the two lines above, we initialize the lists for 
# the solution of the equation i.e the values for tau. 
# I know this was doable with one line of code but when 
# I started doing this exercise I split it into two cases
# hence the two lines of code.

list_tau=list_tau_1+list_tau_2

# Here we combine the two lists into one list.

x=list_R_0
y=list_tau

# Here, we define the x and y axis variables for 
# matplotlib.pyplot, our grpahing library.

plt.plot(x,y)

# We then make the library to plot the variables as x and y axis. 

plt.xlabel('R_0')
plt.ylabel('tau')

# For convenience, we add labels to our graph making it easier to distinguish
# which axis represents which value.

plt.show() 

# Finally we get our graph drawn. 



    