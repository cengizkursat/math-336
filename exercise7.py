import numpy as np 
import math  

# We import 2 Python 3 libraries here. Numpy is for matrix calculations
# and math is for the representation of numbers such as square root of 
# two.

def deflation(matrix):
    eigenvalues=np.linalg.eig(matrix)
    eigenvalue=eigenvalues[0]
    eigenvector=eigenvalues[1][0]
    
# Then we define our function for deflation process. Firstly,
# with the help of linalg.eig in numpy, we find both the eigen-
# values and eigenvectors of our matrix which is the only parameter
# for our function. 
    
    if eigenvector[0]!=0:
        beta=eigenvector[0]/abs(eigenvector[0])
    else:
        beta=1

# Then we define beta according to the directives in the assignment.

    alpha=math.sqrt(2)/np.linalg.norm(np.matrix(eigenvector)-beta*np.matrix([1,0,0,0]))
    v=alpha*(np.matrix(eigenvector)-beta*np.matrix([1,0,0,0]))

# We do the definitions of alpha and v again according to the assignment.

    v_herm=v.getH()
    U=np.identity(4)-np.matmul(v,v_herm)

# Then with the help of numpy method getH(), we get the Hermitian of v and with 
# this knowledge we calculate U again according to the definition in the assingment
# page.

    U_herm=U.getH()

# We then get the Hermitian of U again with getH().

    final=np.matmul(U,matrix)
    final=np.matmul(final,U_herm)
    
# Then we calculate UAU*. 

    final=np.array(final)
    
# Here we convert the numpy matrix to a numpy 4-dimensional array because without
# this conversion for a reason I do not know the code gives an error.

    A_tilda=np.zeros((3,3))

# We then initialize A_tilda as an empty numpy 3-dimensional array.

    for i in range (1,4):
        for j in range (1,4):
            A_tilda[i-1][j-1]=final[i][j]
    
# We then omit the first row and column of final (i.e UAU*) and write the 
# values into our A_tilda 3x3 matrix.

    return U

# Then we return the matrix U.

A=np.array([[1,2,3,5],[2,1,4,6],[3,4,1,7],[5,6,7,1]])
print(deflation(A))