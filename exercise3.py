import numpy as np 
import random

def list_replacer(p,a,b):
    a_index=p.index(a)
    b_index=p.index(b)
    temp=a
    a=b 
    b=temp
    p[a_index]=a 
    p[b_index]=b
    return p

def permutator(p):
    b=np.zeros((len(p),len(p)))
    j=0
    for i in p:
        b[j][i-1]=1 
        j+=1
    return b

def gaussian_pivot(matrix):
    p=[1,2,3,4]
    pivot_list=[]
    elimination_factors=[]
    
    for i in p:
        pivot_list.append(abs(matrix[i-1][0]))
    
    x=max(pivot_list)
    
    p=list_replacer(p,1,pivot_list.index(x)+1)
    permutator_matrix=permutator(p)
    matrix=np.matmul(permutator_matrix,matrix)
    
    for i in range (1,4):
        if x!=0:
            elimination_factors.append(matrix[i][0]/x)
        else:
            elimination_factors.append(0)
    
    for i in range (1,4):
        for j in range (4):
            matrix[i][j]=matrix[i][j]-elimination_factors[i-1]*matrix[0][j]
    
    np.set_printoptions(suppress=True)
    print("After 1st step, our matrix is: \n {}".format(matrix))
    
    matrix_3x3=np.empty((3,3))
    for i in range (1,4):
        for j in range (1,4):
            matrix_3x3[i-1][j-1]=matrix[i][j]
    
    p=[1,2,3]
    pivot_list=[]
    elimination_factors=[]
    
    for i in p:
        pivot_list.append(abs(matrix_3x3[i-1][0]))
    
    x=max(pivot_list)
    
    p=list_replacer(p,1,pivot_list.index(x)+1)
    permutator_matrix=permutator(p)
    matrix_3x3=np.matmul(permutator_matrix,matrix_3x3)
    
    for i in range (1,3):
        if x!=0:
            elimination_factors.append(matrix_3x3[i][0]/x)
        else:
            elimination_factors.append(0)
    
    for i in range (1,3):
        for j in range (3):
            matrix_3x3[i][j]=matrix_3x3[i][j]-elimination_factors[i-1]*matrix_3x3[0][j]
    
    for i in range (1,4):
        for j in range (1,4):
            matrix[i][j]=matrix_3x3[i-1][j-1]
    
    np.set_printoptions(suppress=True)
    print("After 2nd step, our matrix is: \n {}".format(matrix))
    
    matrix_2x2=np.empty((2,2))
    p=[1,2]
    pivot_list=[]
    elimination_factors=[] 

    for i in p:
        pivot_list.append(abs(matrix_2x2[i-1][0]))
    
    x=max(pivot_list)

    p=list_replacer(p,1,pivot_list.index(x)+1)
    permutator_matrix=permutator(p)
    matrix_2x2=np.matmul(permutator_matrix,matrix_2x2)

    for i in range (1,2):
        if x!=0:
            elimination_factors.append(matrix_2x2[i][0]/x)
        else:
            elimination_factors.append(0)

    for i in range (1,2):
        for j in range (2):
            matrix_2x2[i][j]=matrix_2x2[i][j]-elimination_factors[i-1]*matrix_2x2[0][j]

    for i in range (2,4):
        for j in range (2,4):
            matrix[i][j]=matrix_2x2[i-2][j-2]
    
    np.set_printoptions(suppress=True)
    print("After the final step, our matrix is: \n {}".format(matrix))
    



a=np.empty((4,4))
for i in range (4):
    for j in range (4):
        a[i][j]=random.randint(0,20)
print("Our matrix before the process is \n {}".format(a))
gaussian_pivot(a)


